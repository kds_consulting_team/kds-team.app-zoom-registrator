App ingests list of people to be registered with a specific Webinar ID. This ID can be retreived from Zoom website or 
via Keboola's Zoom Extractor. The app processes the registration to specific webinar by utilizing a Zoom JWT Application
registered via https://marketplace.zoom.us/user/build. 
This app is necessary to retrieve an API token. 

Only registrants (unique combination of Webinar ID and email) that have not yet been registered for given Webinar ID 
will be registered and notified via email. 