# Zoom Webinar Registrator

Application ingests a list of users in specific format and registers them for selected Webinar.
The registration is processed uniquely for combination of Webinar ID and email address. If the email address
has already been registered for the Webinar ID, the registration will not be processed again (and thus the user
will not receive the registration email multiple times.)

## Prerequisite
In order to use this app you need to authorize the Zoom account via oAuth in the Authorization tab.

## Required input
Application require 1 (one) input mapping. The data-set in input mapping must include following columns:
- webinar_id (mandatory)	
- email (mandatory)
- first_name (mandatory)
- last_name	(mandatory)
- address	
- city
- country	
- zip	
- state	
- phone	
- industry	
- org	
- job_title	
- purchasing_time_frame	(allowed values: Within a month, 1-3 months, 4-6 months, More than 6 months, No timeframe)
- role_in_purchase_process (allowed values: Decision Maker, Evaluator/Recommender, Influencer, Not involved)
- no_of_employees (allowed values: 1-20, 21-50, 51-100, 101-500, 500-1000, 1001-5000, 5001-10000, More than 10000)
- comments

Refer to Zoom API documentation for more details: https://developers.zoom.us/docs/api/
