#### Application requires 1 (one) input mapping. 
The data-set in input mapping must include following columns:
- __webinar_id__ (mandatory)	
- __email__ (mandatory)
- __first_name__ (mandatory)
- __last_name__	(mandatory)

Optionally, you can add another columns as follows:
- address	
- city
- country	
- zip	
- state	
- phone	
- industry	
- org	
- job_title	
- purchasing_time_frame	(allowed values: Within a month, 1-3 months, 4-6 months, More than 6 months, No timeframe)
- role_in_purchase_process (allowed values: Decision Maker, Evaluator/Recommender, Influencer, Not involved)
- no_of_employees (allowed values: 1-20, 21-50, 51-100, 101-500, 500-1000, 1001-5000, 5001-10000, More than 10000)
- comments    
---
#### Authorization
In order to use this app you need to authorize the Zoom account via oAuth in the Authorization tab.