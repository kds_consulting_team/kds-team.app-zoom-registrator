'''
Template Component main class.

'''

import logging
import csv

from keboola.component import ComponentBase
from keboola.component.exceptions import UserException

from pyzoom import ZoomClient, refresh_tokens

# configuration variables
KEY_API_TOKEN = '#api_token'
STATE_REFRESH_TOKEN = "#refresh_token"


# #### Keep for debug
KEY_STDLOG = 'stdlogging'
KEY_DEBUG = 'debug'
MANDATORY_PARS = []
MANDATORY_IMAGE_PARS = []

MANDATORY_FIELDS = ['webinar_id', 'first_name', 'last_name', 'email']
OPTIONAL_FIELDS = ['address', 'city', 'country', 'zip', 'state', 'phone',
                   'industry', 'org', 'job_title', 'purchasing_time_frame',
                   'role_in_purchase_process', 'no_of_employees', 'comments']

APP_VERSION = '0.1.6'


class Component(ComponentBase):

    def __init__(self, debug=False):
        super().__init__()

        if not self.configuration.oauth_credentials:
            raise UserException("The configuration is not authorized. Please authorize the configuration first.")

        authorization = self.configuration.oauth_credentials
        refresh_token = self.get_state_file().get(STATE_REFRESH_TOKEN, [])

        if refresh_token:
            logging.info("Refresh token loaded from state file")
        else:
            refresh_token = authorization.data.get("refresh_token")

        refreshed_tokens = refresh_tokens(authorization['appKey'], authorization['appSecret'], refresh_token)

        self.write_state_file({STATE_REFRESH_TOKEN: refreshed_tokens.get("refresh_token")})
        self.client = ZoomClient(refreshed_tokens.get("access_token"), refreshed_tokens.get("refresh_token"))

    def run(self):
        '''
        Main execution code
        '''

        in_tables = self.get_input_tables_definitions()

        if len(in_tables) == 0:
            logging.error('There is no table specified on the input mapping. Please provide an input table.')
            exit(1)

        if len(in_tables) > 1:
            logging.error('There is more than 1 (one) table specified on the input mapping.')
            exit(1)

        logging.info("Loading dataset...")
        for table in in_tables:
            table_name = table.full_path

        # Find mandatory and other allowed fields' positions in input file
        with open(table_name, 'r') as f:
            reader = csv.DictReader(f, quotechar='"')

            for registrant in reader:

                missing_keys = set(MANDATORY_FIELDS) - set(registrant.keys())
                if missing_keys:
                    raise UserException(f"[ERROR] Following mandatory fields are missing: {', '.join(missing_keys)}")

                missing_optional_keys = set(OPTIONAL_FIELDS) - set(registrant.keys())
                if missing_optional_keys:
                    logging.info(f"[INFO] Following optional fields are missing: {', '.join(missing_optional_keys)}")

        # Get already registered users
        webinar_ids = []
        with open(table_name, 'r') as f:
            reader = csv.DictReader(f, quotechar='"')
            for registrant in reader:
                webinar_ids.append(registrant.get('webinar_id'))

        webinar_ids = list(dict.fromkeys(webinar_ids))
        already_registrant = []
        for webinar_id in webinar_ids:
            try:
                registered_res = self.client.raw.get("/webinars/" + webinar_id + "/registrants")
                for registrant in registered_res.json().get('registrants', []):
                    already_registrant.append(webinar_id + registrant.get('email'))

            except Exception as e:
                logging.error(f"[ERROR] When obtaining details of webinar: {webinar_id} failed with error: {e} ")

        with open(table_name, 'r') as f:
            reader = csv.DictReader(f, quotechar='"')

            for registrant in reader:
                try:
                    webinar_id = registrant.get('webinar_id')
                    reg_email = registrant.get('email')
                    webinar_registrant_id = webinar_id + reg_email.replace('"', '')
                    if webinar_registrant_id not in already_registrant:
                        url = "/webinars/" + webinar_id + "/registrants"

                        try:
                            self.client.raw.post(url, body=registrant, raise_on_error=True)

                            logging.info(f"[INFO] Registrant {reg_email} successfully "
                                         f"registered to webinar {webinar_id}.")

                        except Exception as e:
                            logging.error(f"[ERROR] Registration failed with error {e} ")

                    else:
                        logging.info(f"[INFO] Registrant {reg_email} was already "
                                     f"registered for webinar {webinar_id} and will not be notified again.")
                except Exception as e:
                    logging.error("[ERROR] Registration failed with error: " + str(e))


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
